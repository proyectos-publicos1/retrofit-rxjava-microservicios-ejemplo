package com.example.serviciocuentas.controllers;

import com.example.serviciocuentas.entities.Cuenta;
import com.example.serviciocuentas.servicies.ICuentaService;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Frank
 */
@RestController
@RequestMapping(value = "/api/v1/cuentas")
public class CuentaController {
    
    @Autowired
    private ICuentaService cuentaService;
    
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Observable<Cuenta> obtenerCuentasPorIdCliente(@RequestParam("idcliente") Integer idCliente) {
        return cuentaService.obtenerCuentasPorIdCliente(idCliente);
    }
    
    @GetMapping(value = "/{id}")
    public Maybe<ResponseEntity<Cuenta>> obtenerPorId(@PathVariable Integer id) {
        return Maybe.just(new Cuenta())
            .map(ResponseEntity::ok)
            .defaultIfEmpty(ResponseEntity.notFound().build());
    }
}
