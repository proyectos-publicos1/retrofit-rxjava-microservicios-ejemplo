package com.example.serviciocuentas.repositories;

import com.example.serviciocuentas.entities.Cuenta;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Frank
 */
public interface ICuentaRepository extends CrudRepository<Cuenta, Integer> {
    
    List<Cuenta> findAllByIdCliente(Integer id);
}
