package com.example.serviciocuentas.servicies;

import com.example.serviciocuentas.entities.Cuenta;
import io.reactivex.Observable;

/**
 *
 * @author Frank
 */
public interface ICuentaService {

    Observable<Cuenta> obtenerCuentasPorIdCliente(Integer idCliente);
}
