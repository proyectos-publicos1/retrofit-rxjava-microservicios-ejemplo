package com.example.serviciocuentas.servicies.impl;

import com.example.serviciocuentas.entities.Cuenta;
import com.example.serviciocuentas.repositories.ICuentaRepository;
import com.example.serviciocuentas.servicies.ICuentaService;
import io.reactivex.Observable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Frank
 */
@Service
public class CuentaServiceImpl implements ICuentaService {

    @Autowired
    private ICuentaRepository cuentaRepository;
    
    @Override
    public Observable<Cuenta> obtenerCuentasPorIdCliente(Integer idCliente) {
        return Observable
            .defer(() -> Observable.fromIterable(cuentaRepository.findAllByIdCliente(idCliente)));
    }

}
