package com.example.servicioclientes.clients;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 *
 * @author Frank
 */
public class ApiNetwork {
    
    private static final String BASE_URL = "http://localhost:8082/api/v1/";
    private static Retrofit retrofit = null;
    
    public static <T> T crearServicio(Class<T> apiClass) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        }
        return retrofit.create(apiClass);
    }
}
