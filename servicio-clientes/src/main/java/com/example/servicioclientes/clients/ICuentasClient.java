package com.example.servicioclientes.clients;

import com.example.servicioclientes.dtos.Cuenta;
import io.reactivex.Observable;
import java.util.List;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 *
 * @author Frank
 */
public interface ICuentasClient {
    // GET http://localhost:8082/api/v1/cuentas?idcliente=1 Observable<Cuenta>
    
    @GET("cuentas")
    Observable<List<Cuenta>> obtenerCuentasPorIdCliente(@Query("idcliente") Integer idCliente);
}
