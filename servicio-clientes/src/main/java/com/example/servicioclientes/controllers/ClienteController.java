package com.example.servicioclientes.controllers;

import com.example.servicioclientes.dtos.Cuenta;
import com.example.servicioclientes.services.IClienteService;
import io.reactivex.Observable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Frank
 */
@RestController
@RequestMapping(value = "/api/v1/clientes")
public class ClienteController {
    
    @Autowired
    private IClienteService clienteService;
    
    // /api/v1/clientes/1/cuentas
    @GetMapping(
        value = "/{id}/cuentas",
        produces = MediaType.APPLICATION_JSON_VALUE)
    public Observable<Cuenta> obtenerCuentasPorId(@PathVariable Integer id) {
        return clienteService.obtenerCuentasPorId(id);
    }
    
}
