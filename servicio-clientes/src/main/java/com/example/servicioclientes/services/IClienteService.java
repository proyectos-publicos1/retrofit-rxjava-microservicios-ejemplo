package com.example.servicioclientes.services;

import com.example.servicioclientes.dtos.Cuenta;
import io.reactivex.Observable;

/**
 *
 * @author Frank
 */
public interface IClienteService {
    
    Observable<Cuenta> obtenerCuentasPorId(Integer id);
    
}
