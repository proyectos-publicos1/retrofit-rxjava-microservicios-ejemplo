package com.example.servicioclientes.services.impl;

import com.example.servicioclientes.clients.ApiNetwork;
import com.example.servicioclientes.clients.ICuentasClient;
import com.example.servicioclientes.dtos.Cuenta;
import com.example.servicioclientes.services.IClienteService;
import io.reactivex.Observable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Frank
 */
@Service
public class ClienteServiceImpl implements IClienteService {
    
    private final ICuentasClient cuentasClient;
    
    public ClienteServiceImpl() {
        cuentasClient = ApiNetwork.crearServicio(ICuentasClient.class);
    }

    @Override
    public Observable<Cuenta> obtenerCuentasPorId(Integer id) {
        return cuentasClient.obtenerCuentasPorIdCliente(id)
            .flatMapIterable(cuentas -> cuentas)
            .flatMap(cuenta -> Observable.just(cuenta));
    }

}
