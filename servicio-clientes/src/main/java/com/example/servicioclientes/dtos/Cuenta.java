package com.example.servicioclientes.dtos;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Frank
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cuenta implements Serializable {

    private Integer id;
    private String numeroCuenta;
    private double saldo;
    private Integer idCliente;
}
